// REDUX
// redux is state management tool

// why REDUX?
// ---> to centralize application data
// ---> component-component communication is easier
// is redux mandatory for react?
// NO ==> only when we feel like we need it
// is redux for react only?
// NO 


// web architecture
//MVC 
// Models views controller
// data flow is bidirectional ==> controller <===> model

// FLUX architecture
// concerns ==> Views,Actions, Dispatchers, Store
// views ====> actions====> dispatchers===> store
// unidirectional data flow

// multiple store
// store update logic resides on store and invoked from dispatchers


// on top FLUX
// REDUX===> state management tool
// concerns ==> Views===> actions====> reducers ===> store
// unidirectional data flow

// single source of truth ==> single store
// store update logic resides on reducers

// dan abramov


// pre-requisities
// js app (react)
// redux ==> independant state container
// react-redux ==> glue (connects react app and redux store)
// middleware ==> redux-thunk,saga
