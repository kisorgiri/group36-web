import React from 'react';
import { ToastContainer } from 'react-toastify';
import { AppRouting } from './app.routing';
import 'react-toastify/dist/ReactToastify.css';
// join react app with redux
import { Provider } from 'react-redux'
import { store } from '../store';


// functional component
// functional component must have return block
// it must return single html node
// return will contain jsx code
// 1st lessom to group jsx code we use parenthesis
// 2nd lesson {} interpolation
// {} is used to provide value inside JSX
export const App = (props) => {
    return (
        <div>
            <Provider store={store}>
                <AppRouting />
                <ToastContainer></ToastContainer>
            </Provider>

        </div>
    )
}






// root component
// this component will export content to be rendered in index.js


// component
// component is basic building block of react
// component is responsible for returning single html node 
// component can be written as class based as well as functional 
// component can be statefull as well as stateless

// statefull component ==>component that maintains data within itself
// stateless component ==> component that doesnot maintains data within itself


// react version  17.02

// react 16.8 
// hooks are introduced from react 16.8
// functional component can be used as statefull compoennt

// class based component for statefull  almost all component
// functional component for stateless almost all component


// glossary
// props and state



// component life cycle

// 3 phase of components life cycle
// init
// update
// destroy

// init ==> 
// constructor()
// render();
// ComponentDidMount();
// data fetch, data prepare


// update phase
// either state change or props change
// render();
// componentDidUpdate(); 
// componentDidUpdate(prevProps,prevState); 


// destroy phase
// componentWillUnMount();




