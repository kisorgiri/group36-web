import React, { Component } from 'react';
import './login.component.css'
import { SubmitButton } from './../../common/submitButton/submitButton.component'
import { Link } from 'react-router-dom';
import { notify } from '../../../utils/notify';
import { handleError } from './../../../utils/error.handler';
import { httpClient } from '../../../utils/httpClient';
import { redirectToDashboard } from '../../../utils/util';


// class based component
// render method is compulsion in class based component
// redner method must have return block it should return single html node

const defaultForm = {
    username: '',
    password: ''
}
export class Login extends Component {

    constructor(props) {
        super(props);
        console.log('at first')
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            remember_me: false,
            isSubmitting: false,
            isValidForm: false,
        }
    }
    componentDidMount() {
        const isRemember = localStorage.getItem('remember_me')
            ? JSON.parse(localStorage.getItem('remember_me'))
            : false
        if (isRemember) {
            this.props.history.push('/dashboard')
        }
    }

    componentDidUpdate(prevProps, prevState) {
        // console.log('prev state chapater>>', prevState.chapter)
        // console.log('once updated>>', this.state.chapter)
    }

    componentWillUnmount() {
        // console.log('once componet is detroyed');
    }

    // TODO learn lifecycle methods

    validateForm = (type, filedName) => {

        if (type === 'submit') {
            let usernameErr;
            let passwordErr;
            let isValidForm = true;
            if (!this.state.data.username) {
                usernameErr = 'required field*';
                isValidForm = false;

            }
            if (!this.state.data.password) {
                passwordErr = 'required field*';
                isValidForm = false;

            }

            this.setState({
                error: {
                    username: usernameErr,
                    password: passwordErr

                }
            })
            return isValidForm;
        }

        let errMsg = this.state.data[filedName]
            ? ''
            : 'required field*';

        this.setState(preState => ({
            error: {
                ...preState.error,
                [filedName]: errMsg
            }
        }))
    }


    submit = (e) => {
        e.preventDefault();

        const isValidForm = this.validateForm('submit');
        if (!isValidForm) return;

        this.setState({
            isSubmitting: true
        })

        // fetch({
        //     url:'',
        //     method:'POST',
        //     body:this.state.data
        // }).then()

        httpClient.POST('/auth/login', this.state.data)
            .then((response) => {
                console.log('resonse >>', response)
                // http response
                // response . data ==> server response
                //web storage
                localStorage.setItem('token', response.data.token);
                localStorage.setItem('user', JSON.stringify(response.data.user))
                localStorage.setItem('remember_me', this.state.remember_me)
                notify.showSuccess(`Welcome ${response.data.user.username}`)
                redirectToDashboard(this.props.history);
            })
            .catch(err => {
                handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    change = (e) => {
        const { type, name, value, checked } = e.target;

        if (name === 'remember_me') {
            return this.setState({
                [name]: checked
            })
        }
        this.setState((prevState) => ({
            data: {
                ...prevState.data,
                [name]: value
            }
        }), () => {
            this.validateForm('change', name);

        })
    }

    render() {
        // try to keep UI logic inside render but not in return
        return (
            <div className="auth-box">
                <h2>Login</h2>
                <p>Please Login to start</p>
                <form className="form-group" onSubmit={this.submit}>
                    <label htmlFor="username">Username</label>
                    <input className="form-control" type="text" name="username" placeholder="Username" id="username" onChange={this.change}></input>
                    <p className="error">{this.state.error.username}</p>
                    <label htmlFor="password">Password</label>
                    <input className="form-control" type="password" id="password" placeholder="Password" onChange={this.change} name="password" ></input>
                    <p className="error">{this.state.error.password}</p>
                    <input type="checkbox" name="remember_me" onChange={this.change}></input>
                    <label> &nbsp;Remember Me</label>
                    <hr />
                    <SubmitButton
                        isSubmitting={this.state.isSubmitting}
                        enabledLabel="login"
                        disabledLabel="loging in..."
                    ></SubmitButton>

                </form>
                <p>Don't have an account?</p>
                <p className="float-left"> Register <Link to="/register">here</Link></p>
                <p className="float-right"><Link to="/forgot_password">forgot password?</Link></p>
            </div>


        )
    }
}
