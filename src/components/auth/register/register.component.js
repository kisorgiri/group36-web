import React, { Component } from 'react';
import { SubmitButton } from '../../common/submitButton/submitButton.component';
import { Link } from 'react-router-dom'
import { notify } from '../../../utils/notify';
import { httpClient } from './../../../utils/httpClient';
import { handleError } from '../../../utils/error.handler';
const defaultForm = {
    name: '',
    username: '',
    password: '',
    confirmPassword: '',
    email: '',
    mobileNumber: '',
    homeNumber: '',
    alterNumber: '',
    gender: '',
    tempAddress: '',
    permanentAddress: '',
    dob: ''
}
export class Register extends Component {

    constructor() {
        super();
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isSubmitting: false,
            isValidForm: false
        }
    }

    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }

    validateForm = fieldName => {
        let errMsg;
        switch (fieldName) {

            case 'username':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].length > 6
                        ? ''
                        : 'username must be 6 characters long'
                    : 'required field *'
                break;

            case 'password':
                errMsg = this.state.data[fieldName]
                    ? this.state.data['confirmPassword']
                        ? this.state.data['confirmPassword'] === this.state.data[fieldName]
                            ? ''
                            : 'password did not match'
                        : this.state.data[fieldName].length > 6
                            ? ''
                            : 'weak password'
                    : 'required field*'

                break;

            case 'confirmPassword':
                errMsg = this.state.data[fieldName]
                    ? this.state.data['password']
                        ? this.state.data['password'] === this.state.data[fieldName]
                            ? ''
                            : 'password didnot match'
                        : this.state.data[fieldName].length > 6
                            ? ''
                            : 'weak password'
                    : 'required field*'
                break;
            case 'email':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].includes('@') && this.state.data[fieldName].includes('.com')
                        ? ''
                        : 'invalid email'
                    : 'required field*'
                break;

            default:
                break;
        }

        this.setState(preState => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        // API call
        // data extraction
        httpClient.POST('/auth/register', this.state.data)
            .then(response => {
                notify.showInfo("Registration successfull please login");
                this.props.history.push('/')
            })
            .catch(err => {
                handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })

    }

    render() {
        // each and every state and props modify huda run huncha
        return (
            <div className="auth-box">
                <h2>Register</h2>
                <p>Please Register to continue</p>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label htmlFor="name" >Name</label>
                    <input className="form-control" type="text" name="name" id="name" placeholder="Name" onChange={this.handleChange} ></input>
                    <label htmlFor="username" >Username</label>
                    <input className="form-control" type="text" name="username" id="username" placeholder="Username" onChange={this.handleChange} ></input>
                    <p className="error">{this.state.error.username}</p>
                    <label htmlFor="password" >Password</label>
                    <input className="form-control" type="password" name="password" id="password" placeholder="Password" onChange={this.handleChange} ></input>
                    <p className="error">{this.state.error.password}</p>
                    <label htmlFor="confirmPassword" >Confirm Password</label>
                    <input className="form-control" type="password" name="confirmPassword" id="confirmPassword" placeholder="Confirm Password" onChange={this.handleChange} ></input>
                    <p className="error">{this.state.error.confirmPassword}</p>
                    <label htmlFor="email" >Email</label>
                    <input className="form-control" type="email" name="email" id="email" placeholder="Email" onChange={this.handleChange} ></input>
                    <p className="error">{this.state.error.email}</p>
                    <label>Gender</label>
                    <br />
                    <input type="radio" name="gender" onChange={this.handleChange} value="male"></input>
                    <label>Male</label> &nbsp;&nbsp;
                    <input type="radio" name="gender" onChange={this.handleChange} value="female"></input>
                    <label> Female</label>&nbsp;&nbsp;
                    <input type="radio" name="gender" onChange={this.handleChange} value="others"></input>
                    <label>Others</label>&nbsp;&nbsp;
                    <br />
                    <label>Date of Birth</label>
                    <input type="date" className="form-control" name="dob" onChange={this.handleChange} required></input>
                    <label>Phone Number</label>
                    <input type="number" name="mobileNumber" className="form-control" onChange={this.handleChange}></input>
                    <label>Home Number</label>
                    <input type="number" name="homeNumber" className="form-control" onChange={this.handleChange}></input>
                    <label>Alternate Number</label>
                    <input type="number" name="alternateNumber" className="form-control" onChange={this.handleChange}></input>
                    <hr />
                    <SubmitButton
                        isSubmitting={this.state.isSubmitting}
                        isDisabled={!this.state.isValidForm}
                    >

                    </SubmitButton>

                </form>
                <p> Already Registered?</p>
                <p>back to <Link to="/">login</Link></p>
            </div>
        )
    }
}
