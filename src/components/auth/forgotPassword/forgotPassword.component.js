import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { handleError } from '../../../utils/error.handler';
import { httpClient } from '../../../utils/httpClient';
import { notify } from '../../../utils/notify';
import { SubmitButton } from './../../common/submitButton/submitButton.component';

export class ForgotPassword extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: '',
            emailErr: '',
            isSubmitting: false
        }
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState(preState => ({
            ...preState,
            [name]: value
        }), () => {

            // form validation
            let errMsg = this.state.email
                ? this.state.email.includes('@') && this.state.email.includes('.com')
                    ? ''
                    : 'invalid email'
                : 'required field*'

            this.setState(preState => ({
                ...preState,
                emailErr: errMsg
            }))
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        const { email } = this.state;
        httpClient.POST('/auth/forgot-password', { email })
            .then(response => {
                notify.showInfo("Password reset link sent to your email please check your inbox");
                this.props.history.push("/");
            })
            .catch(err => {
                handleError(err)
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        return (
            <div className="auth-box">
                <h2>Forgot Password</h2>
                <p>Please use registered email to reset your password</p>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label>Email</label>
                    <input type="text" className="form-control" name="email" placeholder="email address here" onChange={this.handleChange}></input>
                    <p className="error">{this.state.emailErr}</p>
                    <hr></hr>
                    <SubmitButton
                        isSubmitting={this.state.isSubmitting}
                        isDisabled={this.state.emailErr || !this.state.email}
                    >

                    </SubmitButton>
                </form>
                <p>back to <Link to="/">login</Link></p>

            </div>
        )
    }
}
