import react from 'react';
export const SubmitButton = (props) => {

    const disabledLabel = props.disabledLabel || 'submitting...';
    const enabledLabel = props.enabledLabel || 'submit'

    let btn = props.isSubmitting
        ? <button disabled className="btn btn-info"> {disabledLabel}</button>
        : <button disabled={props.isDisabled} type="submit" className="btn btn-primary"> {enabledLabel}</button>
    return btn;
}
