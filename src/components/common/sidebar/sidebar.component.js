import React from 'react'
import { NavLink } from 'react-router-dom'
import './sidebar.component.css'

export const Sidebar = (props) => {

    return (
        <div className="sidebar">
            <NavLink activeClassName="selectedSidebar" to="/add_product"> Add Product</NavLink>
            <NavLink activeClassName="selectedSidebar" to="/view_products"> View Products</NavLink>
            <NavLink activeClassName="selectedSidebar" to="/search_product"> Search Product</NavLink>
            <hr></hr>
            <NavLink activeClassName="selectedSidebar" to="/notifications"> Notifications</NavLink>
            <hr></hr>
            <NavLink activeClassName="selectedSidebar" to="/messages"> Messages</NavLink>
        </div>
    )

    // fragments which used to group the node without additional div
    // syntax <> </> 
    // <React.Fragment>
}
