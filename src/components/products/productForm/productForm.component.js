import React, { Component } from 'react'
import { formatDate } from '../../../utils/dateUtil'
import { SubmitButton } from './../../common/submitButton/submitButton.component'
const IMG_URL = process.env.REACT_APP_IMG_URL;

const defaultForm = {
    name: '',
    category: '',
    description: '',
    quantity: '',
    isReturnEligible: '',
    discountedItem: '',
    discountType: '',
    discountValue: '',
    warrentyStatus: '',
    warrentyPeriod: '',
    manuDate: '',
    expiryDate: '',
    purchasedDate: '',
    salesDate: '',
    tags: '',
    offers: '',
    color: '',
    modelNo: '',
    price: '',
    size: '',
    brand: ''
}

export default class ProductForm extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isValidForm: false,
            filesToUpload: [],
            filesToPreview: [], //purano 2ta , naya 2ta
            filesToRemove: []
        }
    }
    componentDidMount() {
        console.log('this .props >>', this.props)
        const { productData } = this.props
        if (productData) {
            this.setState({
                data: {
                    ...defaultForm,
                    ...productData,
                    discountedItem: productData.discount && productData.discount.discountedItem
                        ? productData.discount.discountedItem
                        : '',
                    discountType: productData.discount && productData.discount.discountType
                        ? productData.discount.discountType
                        : '',
                    discountValue: productData.discount && productData.discount.discountValue
                        ? productData.discount.discountValue
                        : '',
                    salesDate: productData.salesDate ? formatDate(productData.salesDate, 'YYYY-MM-DD') : '',
                    purchasedDate: productData.purchasedDate ? formatDate(productData.purchasedDate, 'YYYY-MM-DD') : '',
                    manuDate: productData.manuDate ? formatDate(productData.manuDate, 'YYYY-MM-DD') : '',
                    expiryDate: productData.expiryDate ? formatDate(productData.expiryDate, 'YYYY-MM-DD') : '',
                }
            })
            this.populateImages(productData.images);
        }
    }

    populateImages = (images = []) => {
        const existingFiles = images.map(img => (
            `${IMG_URL}/${img}`
        ))
        this.setState({
            filesToPreview: existingFiles
        })
    }

    handleChange = e => {
        let { name, value, type, checked, files } = e.target;

        if (type === 'file') {
            const { filesToUpload } = this.state;
            filesToUpload.push(files[0]);
            this.setState({
                filesToUpload,
            })
            return
        }
        if (type === 'checkbox') {
            value = checked;
        }
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }

    validateForm = fieldName => {

        let errMsg;
        switch (fieldName) {
            case 'name':
            case 'category':
                errMsg = this.state.data[fieldName]
                    ? ''
                    : 'required field*'

                break;
            default:
                break;
        }
        this.setState(pre => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        const { data, filesToRemove } = this.state;
        data.filesToRemove = filesToRemove;
        // return;
        this.props.submitAction(data, this.state.filesToUpload);
    }

    removeExistingImages = (index, img) => {
        const { filesToPreview, filesToRemove } = this.state;
        filesToRemove.push(img);

        filesToPreview.splice(index, 1); // previous images as well

        this.setState({
            filesToPreview,
            filesToRemove
        })
    }

    removeNewImages = (index) => {
        const { filesToUpload } = this.state;

        filesToUpload.splice(index, 1); // previous images as well

        this.setState({
            filesToUpload
        })
    }

    render() {
        const mode = this.props.isEdit ? 'Update' : 'Add';
        const { data, error } = this.state;
        return (
            <>
                <h2>{mode} Product</h2>
                <p>Plese {mode} necessary details</p>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label>Name</label>
                    <input type="text" value={data.name} name="name" className="form-control" onChange={this.handleChange} placeholder="Name"></input>
                    <p className="error">{error.name}</p>
                    <label>Description</label>
                    <textarea rows="8" value={data.description} name="description" className="form-control" onChange={this.handleChange} placeholder="Description"></textarea>
                    <label>Category</label>
                    <input type="text" value={data.category} name="category" className="form-control" onChange={this.handleChange} placeholder="Category"></input>
                    <p className="error">{error.category}</p>
                    <label>Brand</label>
                    <input type="text" value={data.brand} name="brand" className="form-control" onChange={this.handleChange} placeholder="Brand"></input>
                    <label>Color</label>
                    <input type="text" value={data.color} name="color" className="form-control" onChange={this.handleChange} placeholder="Color"></input>
                    <label>Price</label>
                    <input type="number" value={data.price} name="price" className="form-control" onChange={this.handleChange} placeholder="Price"></input>
                    <label>Quantity</label>
                    <input type="text" value={data.quantity} name="quantity" className="form-control" onChange={this.handleChange} placeholder="Quantity"></input>
                    <label>Size</label>
                    <input type="text" value={data.size} name="size" className="form-control" onChange={this.handleChange} placeholder="Size"></input>
                    <input type="checkbox" name="isReturnEligible" checked={data.isReturnEligible} onChange={this.handleChange}></input>
                    <label> &nbsp;Return Eligible</label>
                    <br />
                    <input type="checkbox" checked={data.discountedItem} name="discountedItem" onChange={this.handleChange}></input>
                    <label> &nbsp;Discounted Item</label>
                    <br />
                    {
                        data.discountedItem && (
                            <>
                                <label>Discount Type</label>
                                <select name="discountType" value={data.discountType} className="form-control" onChange={this.handleChange} >
                                    <option value="">(Select Type)</option>
                                    <option value="percentage">Percentage</option>
                                    <option value="quantity">Quantity</option>
                                    <option value="value">Value</option>
                                </select>
                                <label>Discount Value</label>
                                <input type="text" value={data.discountValue} name="discountValue" className="form-control" onChange={this.handleChange} placeholder="Discount Value"></input>
                            </>
                        )
                    }
                    <input type="checkbox" checked={data.warrentyStatus} name="warrentyStatus" onChange={this.handleChange} ></input>
                    <label> &nbsp;Warrenty Status</label>
                    <br></br>
                    {
                        data.warrentyStatus && (
                            <>
                                <label>Warrenty Period</label>
                                <input type="text" value={data.warrentyPeriod} name="warrentyPeriod" className="form-control" onChange={this.handleChange} placeholder="Warrenty Period"></input>
                            </>
                        )
                    }
                    <label>Tags</label>
                    <input type="text" name="tags" value={data.tags} className="form-control" onChange={this.handleChange} placeholder="Tags"></input>
                    <label>Offers</label>
                    <input type="text" name="offers" value={data.offers} className="form-control" onChange={this.handleChange} placeholder="Offers"></input>
                    <label>Model No</label>
                    <input type="text" name="modelNo" value={data.modelNo} className="form-control" onChange={this.handleChange} placeholder="Model No"></input>
                    <label>Sales Date</label>
                    <input type="date" name="salesDate" value={data.salesDate} className="form-control" onChange={this.handleChange} ></input>
                    <label>Purchased Date</label>
                    <input type="date" name="purchasedDate" value={data.purchasedDate} className="form-control" onChange={this.handleChange} ></input>
                    <label>Manu Date</label>
                    <input type="date" name="manuDate" value={data.manuDate} className="form-control" onChange={this.handleChange} ></input>
                    <label>Expiry Date</label>
                    <input type="date" name="expiryDate" value={data.expiryDate} className="form-control" onChange={this.handleChange} ></input>
                    <label>Choose Images</label>
                    <input type="file" className="form-control" onChange={this.handleChange} accept="image/png, image/gif, image/jpeg"></input>
                    {/* for existing images */}
                    {
                        this.state.filesToPreview.map((img, index) => (
                            <div key={index}>
                                <br />
                                <img src={img} alt="selected_image.png" width="200px"></img>
                                <i
                                    onClick={() => this.removeExistingImages(index, img)}
                                    style={{ color: 'red', marginLeft: '10px' }}
                                    className="fa fa-trash"
                                    title="remove selected image"
                                ></i>

                            </div>
                        ))
                    }
                    {/* for new images */}
                    {
                        this.state.filesToUpload.map((img, index) => (
                            <div key={index}>
                                <br />
                                <img src={URL.createObjectURL(img)} alt="selected_image.png" width="200px"></img>
                                <i
                                    onClick={() => this.removeNewImages(index)}
                                    style={{ color: 'red', marginLeft: '10px' }}
                                    className="fa fa-trash"
                                    title="remove selected image"
                                ></i>

                            </div>
                        ))
                    }
                    <hr></hr>

                    <SubmitButton
                        isSubmitting={this.props.isSubmitting}
                        isDisabled={!this.state.isValidForm}
                    ></SubmitButton>
                </form>
            </>
        )
    }
}
