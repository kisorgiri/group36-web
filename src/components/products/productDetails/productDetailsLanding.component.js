import React, { Component } from 'react'
import { connect } from 'react-redux';
import { fetchProduct_ac, addReview_ac } from '../../../actions/product.ac';
import AddReview from './addReviews/addReview.component'
import { ProductDetails } from './details/details.component'

class ProductDetailsLanding extends Component {

    componentDidMount() {
        this.productId = this.props.match.params['id'];
        this.props.fetch_product(this.productId)

    }

    addReview = (data) => {
        console.log('data is >>', data)
        this.props.addReview_ac(this.productId, data)
    }

    render() {
        return (
            <>
                <ProductDetails isLoading={this.props.isLoading} product={this.props.product}></ProductDetails>
                <AddReview isSubmitting={this.props.isSubmitting} submit={this.addReview}></AddReview>

            </>
        )
    }
}

const mapStateToProps = rootStore => ({
    isLoading: rootStore.product.loading,
    product: rootStore.product.product,
    isSubmitting: rootStore.product.submitting
})

const mapDispatchToProps = dispatch => ({
    fetch_product: (id) => dispatch(fetchProduct_ac(id)),
    addReview_ac: (id, data) => dispatch(addReview_ac(id, data))
})

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetailsLanding)
