import React from 'react'
import { Loader } from '../../../common/loader/loader.componet'
import ImageGallery from 'react-image-gallery';
import "react-image-gallery/styles/css/image-gallery.css";
import { relativeTime } from '../../../../utils/dateUtil';

const IMG_URL = process.env.REACT_APP_IMG_URL;


const images = [
    {
        original: 'https://picsum.photos/id/1018/1000/600/',
        thumbnail: 'https://picsum.photos/id/1018/250/150/',
    },
    {
        original: 'https://picsum.photos/id/1015/1000/600/',
        thumbnail: 'https://picsum.photos/id/1015/250/150/',
    },
    {
        original: 'https://picsum.photos/id/1019/1000/600/',
        thumbnail: 'https://picsum.photos/id/1019/250/150/',
    },
];

const getImages = (images = []) => {

    return images.map(img => ({
        original: `${IMG_URL}/${img}`,
    }))

}

export const ProductDetails = ({ product, isLoading }) => {

    return (
        <>
            <h2>Product Details</h2>
            <div className="row">
                <div className="col-md-4">
                    <ImageGallery items={getImages(product.images)} />
                </div>
                <div className="col-md-8">
                    <h3>{product.name} Details</h3>
                    <p>{product.description}</p>
                    <div>
                        <h3>Reviews</h3>
                        {
                            (product.reviews || []).map(review => (
                                <div key={review._id}>
                                    <h3>{review.user.username}</h3>
                                    <p>{review.point}</p>
                                    <p>{review.message}</p>
                                    <small>{relativeTime(review.createdAt)}</small>
                                </div>
                            ))
                        }
                    </div>

                </div>
            </div>
        </>
    );
}
