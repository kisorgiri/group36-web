import React, { Component } from 'react'
import { SubmitButton } from '../../../common/submitButton/submitButton.component';

export default class AddReview extends Component {
    constructor(props) {
        super(props)

        this.state = {
            reviewPoint: '',
            reviewMessage: ''
        }
    }
    submit = (e) => {
        e.preventDefault();
        this.props.submit(this.state)
        this.setState({
            reviewPoint: '',
            reviewMessage: ''
        })
    }

    handelChange = e => {
        const { name, value } = e.target;
        this.setState({

            [name]: value
        })
    }

    render() {
        console.log('is submitting check >>', this.props.isSubmitting)
        return (
            <>
                <h2>Add Review</h2>
                <form className="form-group" noValidate onSubmit={this.submit}>
                    <label>Point</label>
                    <input type="number" value={this.state.reviewPoint} name="reviewPoint" onChange={this.handelChange} className="form-control"></input>
                    <label>Message</label>
                    <input type="text"  value={this.state.reviewMessage} name="reviewMessage" placeholder="Review Message Here..." onChange={this.handelChange} className="form-control"></input>
                    <hr />
                    <SubmitButton
                        isSubmitting={this.props.isSubmitting}
                    ></SubmitButton>
                </form>
            </>
        )
    }
}
