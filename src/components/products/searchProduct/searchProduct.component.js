import React, { Component } from 'react'
import { httpClient } from './../../../utils/httpClient'
import { notify } from './../../../utils/notify'
import { handleError } from './../../../utils/error.handler'
import { SubmitButton } from './../../common/submitButton/submitButton.component'
import { ViewProducts } from './../viewProducts/viewProducts.component'

const defaultForm = {
    category: '',
    name: '',
    color: '',
    minPrice: '',
    maxPrice: '',
    fromDate: '',
    toDate: '',
    brand: '',
    tags: '',
    multipleDateRange: ''
}

export class SearchProduct extends Component {

    constructor(props) {
        super(props)

        this.state = {
            data: {
                ...defaultForm,
            },
            error: {
                ...defaultForm
            },
            categories: [],
            names: [],
            allProducts: [],
            isSubmitting: false,
            isValidForm: false,
            isLoading: false,
            searchResults: []

        }
    }
    componentDidMount() {
        this.setState({
            isLoading: true
        })
        httpClient.POST('/product/search', {})
            .then(response => {
                let categories = [];
                // response data will with records
                (response.data || []).forEach(item => {
                    if (!categories.includes(item.category)) {
                        categories.push(item.category);
                    }
                })
                this.setState({
                    categories,
                    allProducts: response.data
                })
            })
            .catch(err => {
                handleError(err);
            })
            .finally(() => {
                this.setState({
                    isLoading: false
                })
            })
    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        const { data } = this.state;
        if (!data.multipleDateRange) {
            data.toDate = data.fromDate;
        }
        httpClient.POST('/product/search', data)
            .then(response => {
                if (!(response.data && response.data.length)) {
                    notify.showInfo("No any product matched your search query");
                }
                // show result in UI
                this.setState({
                    searchResults: response.data
                })
            })
            .catch(err => {
                handleError(err);
            })
            .finally(() => {
                this.setState({
                    isSubmitting: false
                })
            })
    }

    handleChange = e => {
        let { name, value, type, checked } = e.target;
        if (name === 'category') {
            this.populateNames(value);
        }
        if (type === 'checkbox') {
            value = checked
        }

        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }))

    }
    resetSearch = () => {
        this.setState({
            searchResults: [],
            data: {
                ...defaultForm
            }
        })
    }

    populateNames = selectedCategory => {
        const names = this.state.allProducts.filter(item => item.category === selectedCategory);
        this.setState({
            names
        })
    }

    render() {
        const { categories, names, isSubmitting, isLoading, data, searchResults } = this.state;
        let content = searchResults && searchResults.length
            ? <ViewProducts
                productData={searchResults}
                resetSearch={this.resetSearch}
            ></ViewProducts>
            : <>
                <h2>Search Product</h2>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label>Category</label>
                    <select className="form-control" name="category" onChange={this.handleChange}>
                        <option value={this.state.data.category}>(Select Category)</option>
                        {
                            isLoading && (
                                <option>loading options...</option>
                            )
                        }

                        {!isLoading && categories.map((cat, index) => (
                            <option key={index} value={cat}>{cat}</option>
                        ))}
                    </select>
                    {
                        data.category && (
                            <>
                                <label>Name</label>
                                <select className="form-control" name="name" onChange={this.handleChange}>
                                    <option value={this.state.data.name}>(Select Name)</option>
                                    {names.map((name, index) => (
                                        <option key={index} value={name.name}>{name.name}</option>
                                    ))}
                                </select>
                            </>
                        )
                    }

                    <label>Brand</label>
                    <input type="text" name="brand" className="form-control" placeholder="Brand" onChange={this.handleChange}></input>
                    <label>Color</label>
                    <input type="text" name="color" className="form-control" placeholder="Color" onChange={this.handleChange}></input>
                    <label>Min Price</label>
                    <input type="number" name="minPrice" className="form-control" onChange={this.handleChange}></input>
                    <label>Max Price</label>
                    <input type="number" name="maxPrice" className="form-control" onChange={this.handleChange}></input>
                    <label>Select Date</label>
                    <input type="date" name="fromDate" className="form-control" onChange={this.handleChange}></input>
                    <input type="checkbox" name="multipleDateRange" onChange={this.handleChange}></input>
                    <label> &nbsp;Multiple Date Range</label>
                    <br />
                    {
                        data.multipleDateRange && (
                            <>
                                <label>To Date</label>
                                <input type="date" name="toDate" className="form-control" onChange={this.handleChange}></input>
                            </>
                        )
                    }

                    <label>Tags</label>
                    <input type="text" name="tags" className="form-control" placeholder="Tags" onChange={this.handleChange}></input>
                    <hr />
                    <SubmitButton

                        isSubmitting={isSubmitting}
                    >

                    </SubmitButton>

                </form>
            </>
        return content
    }
}
