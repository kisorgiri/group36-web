import React, { Component } from 'react'
import { handleError } from '../../../utils/error.handler'
import { httpClient } from '../../../utils/httpClient'
import { notify } from '../../../utils/notify'
import { Loader } from '../../common/loader/loader.componet'
import ProductForm from '../productForm/productForm.component'

export class EditProduct extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isSubmitting: false,
            isLoading: false,
            product: {}
        }
    }

    componentDidMount() {
        this.setState({
            isLoading: true
        })
        this.productId = this.props.match.params['id'];
        httpClient.GET(`/product/${this.productId}`, true)
            .then(response => {
                this.setState({
                    product: response.data
                })
            })
            .catch(err => {
                handleError(err);
            })
            .finally(() => {
                this.setState({
                    isLoading: false
                })
            })
    }

    edit = (data, files) => {
        this.setState({
            isSubmitting: true
        })
        data.vendor = data.vendor._id;
        httpClient.UPLOAD('PUT', `/product/${this.productId}`, data, files)
            .then(response => {
                notify.showInfo("proudct updated");
                this.props.history.push('/view_products');
            })
            .catch(err => {
                handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })

    }

    render() {
        let content = this.state.isLoading
            ? <Loader></Loader>
            : <ProductForm
                isSubmitting={this.state.isSubmitting}
                isEdit={true}
                submitAction={this.edit}
                productData={this.state.product}
            ></ProductForm>
        return content;
    }
}
