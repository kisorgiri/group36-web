import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { formatDate } from '../../../utils/dateUtil'
import { Loader } from '../../common/loader/loader.componet'
import { connect } from 'react-redux'
import { changePageNumber, fetchProducts_ac, removeProduct_ac } from '../../../actions/product.ac'

const IMG_URL = process.env.REACT_APP_IMG_URL;

class ViewProductsComponent extends Component {

    componentDidMount() {
        console.log('porps >>', this.props)
        const { pageNumber, pageSize } = this.props;
        this.props.fetchProducts_ac({ pageNumber, pageSize });
        const { productData } = this.props;
        if (productData) {
            return this.setState({
                products: productData
            })
        }
    }

    removeProudct = (id) => {
        let confirmation = window.confirm('Are you sure to remove?')
        if (confirmation) {
            this.props.removeProduct_ac(id)
        }

    }

    editProduct = id => {
        this.props.history.push(`/edit_product/${id}`)
    }

    changePage = (val) => {
        let currentPage = this.props.pageNumber;
        console.log('current page >>', currentPage)
        if (val === 'next') {
            currentPage++;
        }
        if (val === 'previous') {
            currentPage--;
        }
        this.props.fetchProducts_ac({
            pageNumber: currentPage,
            pageSize: this.props.pageSize
        });
        this.props.changePageNumber(currentPage)

    }

    render() {

        let content = this.props.loadingProduct
            ? <Loader></Loader>
            : <table className="table">
                <thead>
                    <tr>
                        <th>S.N</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Price</th>
                        <th>Created At</th>
                        <th>Tags</th>
                        <th>Images</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        (this.props.products || []).map((product, index) => (
                            <tr key={product._id}>
                                <td>{index + 1}</td>
                                <td><Link to={`product_details/${product._id}`}>{product.name}</Link> </td>
                                <td>{product.category}</td>
                                <td>{product.price}</td>
                                <td> {formatDate(product.createdAt, 'ddd YYYY/MM/DD')}</td>
                                <td> {product.tags.toString()}</td>
                                <td>
                                    <img src={`${IMG_URL}/${product.images[0]}`} alt="product_image" width="200px"></img>
                                </td>
                                <td>
                                    <button onClick={() => this.editProduct(product._id)} className="btn btn-default" title="Edit Product">
                                        <i style={{ color: 'blue' }} className="fa fa-pencil"></i>
                                    </button>
                                    <button onClick={() => this.removeProudct(product._id, index)} className="btn btn-default" title="Remove Product">
                                        <i style={{ color: 'red' }} className="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                        ))
                    }

                </tbody>
            </table>
        return (
            <>
                <h2>View Products</h2>
                {this.props.productData && (
                    <button className="btn btn-info" onClick={() => this.props.resetSearch()}> search again</button>
                )}
                {content}
                <hr />
                {
                    this.props.pageNumber !== 1 && (

                        <button className="btn btn-success" onClick={() => this.changePage('previous')}>previous </button>
                    )
                }
                {this.props.products.length > 0 && (
                    <button style={{ marginLeft: '10px' }} className="btn btn-success" onClick={() => this.changePage('next')}>next</button>

                )}

            </>
        )
    }
}

// mapStateToProps
// incoming data for connected componented
const mapStateToProps = rootStore => ({
    pageNumber: rootStore.product.pageNumber,
    pageSize: rootStore.product.pageSize,
    products: rootStore.product.records,
    loadingProduct: rootStore.product.loading,
})


// mapDipatch to props
// component bata k k action trigger garne 
const mapDispatchToProps = dispatch => ({
    changePageNumber: (newPageNumber) => dispatch(changePageNumber(newPageNumber)),
    fetchProducts_ac: (params) => dispatch(fetchProducts_ac(params)),
    removeProduct_ac: (id) => dispatch(removeProduct_ac(id))
})
export const ViewProducts = connect(mapStateToProps, mapDispatchToProps)(ViewProductsComponent)
