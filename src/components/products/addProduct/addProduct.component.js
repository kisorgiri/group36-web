import React, { Component } from 'react'
import ProductForm from '../productForm/productForm.component'
import { httpClient } from './../../../utils/httpClient'
import { notify } from './../../../utils/notify'
import { handleError } from './../../../utils/error.handler'

export class AddProduct extends Component {
    constructor() {
        super()

        this.state = {
            isSubmitting: false
        }
    }

    add = (data, files) => {
        this.setState({
            isSubmitting: true
        })
        httpClient.UPLOAD('POST', '/product', data, files)
            .then(response => {
                notify.showSuccess("Product Added Successfully")
                this.props.history.push('/view_products')
            })
            .catch(err => {
                handleError(err)
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        return (

            <ProductForm
                isEdit={false}
                isSubmitting={this.state.isSubmitting}
                submitAction={this.add}
            ></ProductForm>
        )
    }
}
