import React, { Component } from 'react'
import * as io from 'socket.io-client';
import './message.component.css'
import { relativeTime } from './../../../utils/dateUtil'
import { notify } from './../../../utils/notify'

const socket_URL = process.env.REACT_APP_SOCKET_URL;

export default class Messages extends Component {
    constructor(props) {
        super(props)

        this.state = {
            messageBody: {
                message: '',
                senderName: '',
                senderId: '',
                receiverName: '',
                receiverId: '',
                time: ''
            },
            messages: [],
            users: []
        }
    }
    componentDidMount() {
        this.socket = io(socket_URL)
        this.currentUser = JSON.parse(localStorage.getItem('user'));
        this.runSocket();
    }

    runSocket = () => {
        this.socket.emit('new-user', this.currentUser.username)
        this.socket.on('reply-message-own', (data) => {
            const { messages } = this.state;
            messages.push(data)
            this.setState({
                messages
            })
        })

        this.socket.on('reply-message', (data) => {
            const { messages, messageBody } = this.state;
            // swap sender and receiver whener receiver recive message
            messageBody.receiverId = data.senderId
            messages.push(data)
            this.setState({
                messages,
                messageBody
            })
        })

        this.socket.on('users', (data) => {
            this.setState({
                users: data
            })
        })
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState(preState => ({
            messageBody: {
                ...preState.messageBody,
                [name]: value
            }
        }))
    }

    selectUser = user => {
        console.log('selected user is >>', user)
        this.setState(preState => ({
            messageBody: {
                ...preState.messageBody,
                receiverId: user.id,
                receiverName: user.name
            }
        }))
    }

    send = (e) => {
        e.preventDefault();
        const { messageBody, users } = this.state;
        // append message body with appropriate information
        if (!messageBody.receiverId) {
            return notify.showInfo("Please select a user to continue")
        }
        messageBody.time = Date.now();
        messageBody.senderName = this.currentUser.username;
        const sender = users.find((user, index) => user.name === this.currentUser.username);
        // socket stuff
        messageBody.senderId = sender.id;
        this.socket.emit('new-message', messageBody)

        this.setState(preState => ({
            messageBody: {
                ...preState.messageBody,
                message: ''
            }
        }))


    }

    render() {
        return (
            <>
                <h2>Messages</h2>
                <p>Let's Chat</p>

                <div className="row">
                    <div className="col-md-6">
                        <ins>Messages</ins>
                        <div className="chat-box">
                            {
                                this.state.messages.map((msg, index) => (
                                    <div key={index}>
                                        <h3>{msg.senderName}</h3>
                                        <p>{msg.message}</p>
                                        <small> {relativeTime(msg.time, 'second')}</small>

                                    </div>
                                ))
                            }
                        </div>
                        <br></br>
                        <form className="form-group" onSubmit={this.send}>
                            <input type="text" value={this.state.messageBody.message} name="message" placeholder="your message here..." className="form-control input-box" onChange={this.handleChange}></input>
                            <button className="btn btn-primary">send</button>
                        </form>

                    </div>
                    <div className="col-md-6">
                        <ins>Users</ins>
                        <div className="chat-box">
                            {this.state.users.map((user, index) => (
                                <React.Fragment key={index}>
                                    <button className="btn btn-default" onClick={() => this.selectUser(user)}>{user.name}</button>
                                    <br />
                                </React.Fragment>
                            ))}
                        </div>

                    </div>
                </div>
            </>
        )
    }
}
