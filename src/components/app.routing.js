import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { ForgotPassword } from './auth/forgotPassword/forgotPassword.component';
import { Login } from './auth/login/login.component'
import { Register } from './auth/register/register.component'
import { ResetPassword } from './auth/resetPassword/resetPassword.component';
import { Header } from './common/header/header.component';
import { Sidebar } from './common/sidebar/sidebar.component';
import { AddProduct } from './products/addProduct/addProduct.component';
import { EditProduct } from './products/editProduct/editProduct.component';
import ProductDetailsLanding from './products/productDetails/productDetailsLanding.component';
import { SearchProduct } from './products/searchProduct/searchProduct.component';
import { ViewProducts } from './products/viewProducts/viewProducts.component';
import Messages from './users/messages/message.component';

const Home = (props) => {
    console.log('props in home >>', props)
    return (
        <p>Home Page</p>
    )
}

const Dashboard = (props) => {
    console.log('props in Dashboard >>', props)
    const name = process.env.REACT_APP_APP_NAME;
    return (
        <div>
            <h2>Welcome to {name} </h2>
            <p>Please use side navigation menu or constact system administrator for support</p>
            <small>dashboard in progress</small>
        </div>

    )
}

const PageNotFound = (props) => {
    return (
        <div>
            <h2>Not Found</h2>
            <img src="./images/notfound.jpg" alt="not_found_image" width="600px"></img>
        </div>
    )
}

const ProtectedRoute = ({ component: Component, ...rest }) => {
    return (

        <Route {...rest} render={(routeProps) => {
            return localStorage.getItem('token')
                ? <div>
                    <Header isLoggedIn={true}></Header>
                    <Sidebar></Sidebar>
                    <div className="main-content">

                        <Component {...routeProps}></Component>
                    </div>
                </div>
                : <Redirect to="/" ></Redirect>
        }}></Route>
    )
}

const PublicRoute = ({ component: Component, ...rest }) => {
    return (

        <Route {...rest} render={(routeProps) => {
            return (<div>
                <Header isLoggedIn={localStorage.getItem('token') ? true : false}></Header>
                <div className="main-content">
                    <Component {...routeProps}></Component>
                </div>
            </div>
            )

        }}></Route>
    )
}

const AuthRoute = ({ component: Component, ...rest }) => {
    return (

        <Route {...rest} render={(routeProps) => {
            return (<div>
                <Header isLoggedIn={false}></Header>
                <div className="main-content">
                    <Component {...routeProps}></Component>
                </div>
            </div>
            )

        }}></Route>
    )
}
export const AppRouting = (props) => {
    return (
        <BrowserRouter>
            <Switch>
                <AuthRoute path="/" exact component={Login}></AuthRoute>
                <AuthRoute path="/reset_password/:token" component={ResetPassword}></AuthRoute>
                <AuthRoute path="/forgot_password" component={ForgotPassword}></AuthRoute>
                <AuthRoute path="/register" component={Register}></AuthRoute>
                <PublicRoute path="/home" component={Home}></PublicRoute>
                <ProtectedRoute path="/dashboard" component={Dashboard}></ProtectedRoute>
                <ProtectedRoute path="/add_product" component={AddProduct}></ProtectedRoute>
                <ProtectedRoute path="/view_products" component={ViewProducts}></ProtectedRoute>
                <ProtectedRoute path="/edit_product/:id" component={EditProduct}></ProtectedRoute>
                <ProtectedRoute path="/search_product" component={SearchProduct}></ProtectedRoute>
                <ProtectedRoute path="/messages" component={Messages}></ProtectedRoute>
                <ProtectedRoute path="/product_details/:id" component={ProductDetailsLanding}></ProtectedRoute>
                <PublicRoute component={PageNotFound}></PublicRoute>
            </Switch>

        </BrowserRouter>
    )
}


// summary
// routing ==>which page to load in which url
// library ==> react-router-dom

// separate component for routing setup
// <BrowserRouter is wrapper for routing configuration>
// ROute is repsonse for config path, component
// Switch ==> once we have path specific component it will not show the empty path component
// Link ,NavLink ==> navigation through click event

// on every components specified by Route
// we have three props
// History  ==> functions to navigate
// Match ==> used to get dynamic values from url [ part of url]
// Location ==> for accessing optional parameters ?.... and data from navigation
