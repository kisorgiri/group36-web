// reducer is plain js function
import { ProductActionTypes } from './../actions/product.ac'
const defaultState = {
    records: [],
    loading: false,
    pageNumber: 1
}
export const ProductReducer = (state = defaultState, action) => {
    // logic to update state(store)
    // action will have what to do when to do

    switch (action.type) {
        case ProductActionTypes.SET_IS_PAGE_NUMBER:
            return {
                ...state,
                pageNumber: action.currentPage
            }

        case ProductActionTypes.SET_IS_LOADING:
            return {
                ...state,
                loading: action.payload
            }
        case ProductActionTypes.SET_IS_SUBMITTING:
            console.log('here for submitting', action.payload)
            return {
                ...state,
                submitting: action.payload
            }
        case ProductActionTypes.PROUDCTS_RECEIVED:
            return {
                ...state,
                records: action.payload
            }

        case ProductActionTypes.PROUDCT_RECEIVED:
            return {
                ...state,
                product: action.payload
            }
        case ProductActionTypes.PRODUCT_DELETED:
            const { records } = state;
            records.forEach((item, index) => {
                if (item._id === action.id) {
                    records.splice(index, 1)
                }
            })
            return {
                ...state,
                records: [...records]
            }

        default:
            return {
                ...state
            }
    }
}

