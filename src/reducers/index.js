import { combineReducers } from 'redux';
import { ProductReducer } from './product.red';

export default combineReducers({
    product: ProductReducer
})
