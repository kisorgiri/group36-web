// store configuration here
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers'

const middleware = [thunk];

const inititalState = {
    product: {
        records: [],
        loading: false,
        submitting: false,
        product: {},
        pageNumber: 1,
        pageSize: 5,
    }
}

export const store = createStore(rootReducer, inititalState, applyMiddleware(...middleware))


// // 
// {
//     notitifcationlist:[],
//     isnotificationLoading:false,
//     products:[],
//     productSubmitting:false,
//     users:[],
//     isLoading:false
// }


// {
//     user:{
//         data:[],
//         isLoading:false
//     },
//     notification:{
//         data:[],
//         isSubmitting:findAllByTestId,
//     },
//     reviews:{

//     },
//     products:{

//     }
// }
