import { handleError } from "../utils/error.handler"
import { httpClient } from "../utils/httpClient"
import { notify } from "../utils/notify"

// middleware usage
export const ProductActionTypes = {
    SET_IS_LOADING: 'SET_IS_LOADING',
    SET_IS_SUBMITTING: 'SET_IS_SUBMITTING',
    SET_IS_PAGE_NUMBER: 'SET_IS_PAGE_NUMBER',
    PROUDCTS_RECEIVED: 'PROUDCTS_RECEIVED',
    PROUDCT_RECEIVED: 'PROUDCT_RECEIVED',
    PRODUCT_DELETED: 'PRODUCT_DELETED',
    REVIEW_ADDED: 'REVIEW_ADDED'
}

export const changePageNumber = (pageNumber) => {
    // enhancer is used here in case of delayed dispatch
    return function (dispatch) {
        dispatch({
            type: ProductActionTypes.SET_IS_PAGE_NUMBER,
            currentPage: pageNumber
        })
    }
}

// action will dispatch an object to reducer
// function fetchProducts_ac(params){
//     return function(dispatch){

//     }
// }

// export const fetchProducts_ac = params => {
//     return dipsatch => {

//     }
// }
export const fetchProducts_ac = (params = {}) => dispatch => {
    console.log('at action', params)
    dispatch({
        type: ProductActionTypes.SET_IS_LOADING,
        payload: true
    })
    httpClient.GET('/product', true, params)
        .then(response => {
            dispatch({
                type: ProductActionTypes.PROUDCTS_RECEIVED,
                payload: response.data
            })
        })
        .catch(err => {
            handleError(err);
        })
        .finally(() => {
            dispatch({
                type: ProductActionTypes.SET_IS_LOADING,
                payload: false
            })
        })

}

export const removeProduct_ac = (id) => dispatch => {
    httpClient.DELETE(`/product/${id}`, true)
        .then(response => {
            notify.showInfo("proudct removed")
            dispatch({
                type: ProductActionTypes.PRODUCT_DELETED,
                id: id
            })
        })
        .catch(err => {
            handleError(err);
        })
}

export const fetchProduct_ac = (id) => dispatch => {
    console.log('here fetch again')
    // dispatch(isLoading(true))
    httpClient.POST(`/product/search`, { _id: id })
        .then(response => {
            dispatch({
                type: ProductActionTypes.PROUDCT_RECEIVED,
                payload: response.data[0]
            })
        })
        .catch(err => {
            handleError(err);
        })
        .finally(() => {
            // dispatch(isLoading(false))
        })
}

export const addReview_ac = (id, data) => dispatch => {
    dispatch(isSubmitting(true));
    httpClient.POST(`/product/add_review/${id}`, data, true)
        .then(response => {
            console.log('here at response')
            dispatch(fetchProduct_ac(id));
        })
        .catch(err => {
            handleError(err);
        })
        .finally(() => {
            dispatch(isSubmitting(false))
        })
}

const isLoading = isLoading => ({
    type: ProductActionTypes.SET_IS_LOADING,
    payload: isLoading
})

const isSubmitting = submitting => ({
    type: ProductActionTypes.SET_IS_SUBMITTING,
    payload: submitting
})

