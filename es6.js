// js ===> versionn===> ECMA
// ECMAScript

// ES 
// stable JS ==> es5

// modern JS ==> es6 and beyond


// topics to be covered
// Object Shorthand
// Object destruction
// spread operator and rest operator
// arrow notation function
// default argument
// import and export
// template literals
// class
// datatype
// 


// Object short hand
// var xyz = []
// var obj = {
//     name: 'kishor',
//     phone: 333,
//     xyz,
// }

// console.log('obj >>', obj)


// Object Destrution

var fish = 'golden fish';

function getSomething() {

    return {
        fruits: ['apple'],
        vegitables: ['potato'],
        fish: 'cat fish',
        abc: 'xyz',
        flower: 'rose'
    }
}

// destruction 
var { fruits } = getSomething();
// own name
var { fish: myFish } = getSomething();

// console.log('result is >>', fruits)
// console.log('flower >>', flower)
console.log('fish is >>', fish)
console.log('mufish is >>', myFish)

// arrow notation function

function welcome(name) {
    return 'hello ' + name;
}

// const welcome = (name) => {
//     return 'hello ' + name;
// }

// // for single argument parenthesis is optional
// const welcome = name => {
//     return 'hello ' + name;
// }

// we can skip(avoid) middlebraces and return statment
// one liner function
// const welcomeArr = name => 'hello ' + name;


// console.log('welcome traditional ',welcome('broadway'))
// console.log('welcome one liner using arrow >>',welcomeArr('infosys'))


// main advantages
// ===>  arrow notation function will inherit parent scope(this)

const laptops = [
    {
        brand: 'dell'
    },
    {
        brand: 'dell'
    }, {
        brand: 'hp'
    },
    {
        brand: 'hp'
    }
]

var dellLaptop = laptops.filter(function (item) {
    if (item.brand === 'dell') {
        return true;
    }
})
const multiply = num1 => num2 => num1 * num2;
console.log('multiply >>', multiply(4)(3))

const dell = laptops.filter(data => data.brand === 'dell');

console.log('dell laptops >>', dellLaptop)

console.log('dell  >>', dell)


// spread operator and rest operator
//  ...

// spread operator
// existing behavour ==> mutable behaviour for array and objet

// var obj1 = {
//     name: 'broadway infosys nepal',
//     address: 'tinkune',
//     shift: 'morning'
// }

// var student = {
//     course: 'mern',
//     shift: 'evening'
// }

// var obj2 = {
//     ...student,
//     ...obj1,
// };
// obj1.phone = '333'

// console.log('obj2 .>>', obj2);
// console.log('obj1', obj1);

// rest operator (destruction)

// var bike = {
//     color: 'blacl',
//     model: 'fz',
//     brand: 'yamaha',
//     cc: 150
// }
// const { model: Model, cc, ...rest } = bike;


// console.log('modle >>', Model)
// console.log('rest >>', rest)


// default argument

// function sendMail(details = {
//     name: 'broadway'
// }) {
//     var message = 'hi ' + details.name + ' welcome to broadway';
//     console.log('message .>', message)
// }

// sendMail()


// function testa(name = 'rupak') {
//     console.log('name ', name);
// }
// testa('sita')


// import and export

// export 

// two types of export
// 1. named export
// 2. default export

// syntax 
// named export
// export const test = 'value'
// export const email =';dlksfj';
// export class Fruits{

// }

// there can be multiple named export within a file


// 2. default export
// syntax
// export default any data type
// eg.export default [];
// eg.export default { }
// eg.export default 'hi'
// eg.export default 33;

// there can be only one default export
// there can be both named and default export

// import
// import totally depends on how it is exported
// if named export 
// syntax
// import {named_export_1, named_export_2} from 'source' // source can be own file, or node_modules packages

// if default export 
// import xyzName(anyName) from 'source'

// if both named and default export presents
// import {name2,name1},DefaultName from 'source

// template literals

// string and string concatination

// syntax ``

// var message = 'hi '+name+'wlecom to broadway
// how are you doing?'

var name = 'kishor'
var text = `hi 
${name} 
welcome to 
broadway`;

console.log('text is >>', text)


// block scope
// let holds block scope // es6


// class 

// class based OOP

// class is basic building block of class based OOP

// class is group of contructor methods and properties (fields);

class Auth {
    isLoggedIn;
    status;
    constructor() {
        this.isLoggedIn = true;
        this.status = 'online'
    }

    getStatus() {
        return this.status;
    }


    setStatus(newStatus) {
        this.status = newStatus;
        return this.status;
    }
}
// inheritance

class B extends Auth {
    bbb;
    constructor() {
        super(); // super call is parent class constructor call
    }

    getBBBB() {

    }
}

// var abc = new B();
// abc.

var xyz = new Auth();
// xyz.


// datatype
// symbol, ==> unique value
// bigint ===> to store large value 


// pre-requisities to learn React
// JSX  ==> javascript extended syntax
// Compiler
// Task Runner

