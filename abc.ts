// interface
// interface is collection of properties that defined object
// eg 
interface da {
    sender: string,
    receiver: string,
    from: string
}

class AuthService {

    isLoggedIn: boolean = true;
    status: string = 'online';
    role: number = 2;
    hobbies: Array<string>;
    interest: string[];
    a: any;

    constructor() {
        // this.role = '333'
        this.sendMail({
            sender: '22',
            receiver: '33',
            from: 'ram'
        })
    }


    sendMail(details: da) {

    }
}

// ts is superset of JS , JS that scales
// JS is weakly typed
// TS is strictly typed
// JS has heterogenous array
// TS has homogenous array
